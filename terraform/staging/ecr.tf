resource "aws_ecr_repository" "repos" {
  count = "${length(var.repositories)}"
  name = "${lookup(var.repositories, format("repo%d", count.index))}"
}

resource "null_resource" "docker-push" {
  provisioner "local-exec" {
    command = <<EOT
      pushd ..
      docker build -t "${aws_ecr_repository.repos.0.name}" . && 
      eval $(aws ecr get-login --no-include-email --region ${var.aws_region}) && 
      docker tag ${aws_ecr_repository.repos.0.name}:latest ${aws_ecr_repository.repos.0.repository_url}:latest &&
      docker push ${aws_ecr_repository.repos.0.repository_url}:latest &&
      popd
    EOT
  }
}
