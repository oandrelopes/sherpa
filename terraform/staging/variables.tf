variable "aws_region" {}
variable "cluster_name" {}
variable "service_name" {}
variable "repositories" { type = "map" }
variable "vpc_id" {}
