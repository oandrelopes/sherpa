resource "aws_cloudwatch_log_group" "project"  {
  name = "sherpa-staging"
  retention_in_days = 30
}
