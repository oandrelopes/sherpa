resource "aws_ecs_task_definition" "service" {
  family = "${var.service_name}"
  container_definitions = "${file("task-definitions/containers.json")}"
}

resource "aws_ecs_service" "ecs_service" {
  name = "${var.service_name}"
  cluster = "${var.cluster_name}"
  task_definition = "${aws_ecs_task_definition.service.arn}"
  desired_count = 1
  deployment_maximum_percent = 100
  deployment_minimum_healthy_percent = 0
}
