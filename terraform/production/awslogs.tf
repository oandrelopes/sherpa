resource "aws_cloudwatch_log_group" "project"  {
  name = "sherpa-prod"
  retention_in_days = 30
}
