terraform {
 backend "s3" {
   bucket         = "terraform-escale"
   key            = "projects/sherpa-prod.state"
   region         = "sa-east-1"
   profile        = "default"
 }
}
