import sbt._

enablePlugins(JavaAppPackaging)
enablePlugins(DockerPlugin)

val projectName = "sherpa"
val releaseVersion = "0.2.0"
val user = "sherpa"

name := projectName
version := releaseVersion
scalaVersion := "2.12.3"

maintainer in Debian := "Escale Development Team <dev@escale.com.br>"

scalacOptions in Compile ++= Seq("-encoding", "UTF-8", "-target:jvm-1.8", "-deprecation", "-feature", "-unchecked", "-Xlog-reflective-calls", "-Xlint")
javacOptions in Compile ++= Seq("-source", "1.8", "-target", "1.8", "-Xlint:unchecked", "-Xlint:deprecation")

val testDependencies = Seq(
  "org.scalactic" %% "scalactic" % "3.0.1",
  "org.scalatest" %% "scalatest" % "3.0.1" % "test",
  "com.github.javafaker" % "javafaker" % "0.13",
  "de.saly" % "javamail-mock2-fullmock" % "0.5-beta4" % "test"
)

val dbsDependencies = Seq(
  "org.scalikejdbc" %% "scalikejdbc" % "3.0.2",
  "org.scalikejdbc" %% "scalikejdbc-config"  % "3.0.2",
  "postgresql" % "postgresql" % "9.1-901.jdbc4",
  "mysql" % "mysql-connector-java" % "5.1.44"
)

val microsoftFilesHandlingDependencies = Seq(
  "org.apache.poi" % "poi" % "3.9",
  "org.apache.poi" % "poi-ooxml" % "3.16",
  "org.apache.poi" % "poi-ooxml-schemas" % "3.9"
)

val generalDependencies = Seq(
  "javax.mail" % "mail" % "1.5.0-b01",
  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "com.typesafe" % "config" % "1.2.1",
  "com.github.tototoshi" %% "scala-csv" % "1.3.5",
  "org.scala-lang.modules" %% "scala-xml" % "1.0.6",
  "org.scalaj" %% "scalaj-http" % "2.3.0"
)

libraryDependencies ++= testDependencies ++ dbsDependencies ++ microsoftFilesHandlingDependencies ++ generalDependencies
dockerBaseImage := "openjdk:jre-alpine"

mainClass in (Compile, run) := Some("br.com.escale.sherpa.Sherpa")

mappings in (Compile, packageBin) ~= {
  _.filterNot {
    case (_, name) =>
      Seq("application.conf").contains(name)
  }
}

bashScriptExtraDefines += s"""addJava "-Dconfig.file=/opt/docker/files/application.conf""""
