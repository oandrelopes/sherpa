![sherpa-wallpaper.jpg](https://i.imgur.com/5BC4AT9.jpg)

# Sherpa ![Build Status](https://travis-ci.com/escaleseo/sherpa.svg?token=543idJdtsNSw2LFAgXjD&branch=development)

Sherpa is Escale's commission data import for each analyst.

This project uses [Scala](https://www.scala-lang.org/) and is containerized by [Docker](https://www.docker.com)... So you don't have to install any dependencies 🤓

## Docker

You can locally build and run it. Make sure you replace EMAIL_USER and EMAIL_PASS in docker-compose.yml for your ones.
In production you also need to set database variables. Check docker-compose.yml and files/start.sh.

```bash
$ docker-compose up -d
```

## Running

```
$ sbt run
```

## Testing

```
$ sbt test
```

## Deploying

To create our build we use [sbt-native-packager](https://www.scala-sbt.org/sbt-native-packager/formats/docker.html), it's focuses on creating a Docker image which can “just run” the application built by SBT.

When you modify something and want to test it using local Docker server, run this following commands:

We build an image of our Scala application

```
$ sbt clean docker:publishLocal // clean our project and builds an image
```

and then we create another one with all dependencies that are included on `docker-compose`

```
$ docker-compose up --build 
``` 
