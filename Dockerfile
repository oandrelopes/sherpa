FROM sherpa:0.2.0

USER root
RUN apk update && \
    apk add --no-cache bash mariadb-client

WORKDIR /opt/docker
RUN mkdir -p /opt/docker/files
COPY files/start.sh files/crontab /opt/docker/files/
RUN /usr/bin/crontab files/crontab
COPY src/main/resources/application.conf /opt/docker/files/
COPY resources/database/database-v1.sql /opt/docker/files/
RUN chmod 755 /opt/docker/files/start.sh

ENTRYPOINT ["/opt/docker/files/start.sh"]
