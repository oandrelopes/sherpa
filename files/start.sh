#!/bin/sh

DBNAME=${DBNAME:=mydb}
DBUSER=${DBUSER:=myuser}
DBPASS=${DBPASS:=mypassword}
DBHOST=${DBHOST:=localhost}
DBPORT=${DBPORT:=3306}
EMAIL_USER=${EMAIL_USER:=myemail@escale.com.br}
EMAIL_PASSWORD=${EMAIL_PASSWORD:=blah}

while ! mysql -h${DBHOST} -u${DBUSER} -p${DBPASS} ${DBNAME} -e"quit"; do
    (mysql -h${DBHOST} -u${DBUSER} -p${DBPASS} ${DBNAME} -e"quit" && sleep 1 && \
    exit 0) || sleep 1 && continue
done

mysql -h${DBHOST} -u${DBUSER} -p${DBPASS} ${DBNAME} < files/database-v1.sql

DBURL="jdbc:mysql:\/\/${DBHOST}:${DBPORT}\/${DBNAME}?useSSL=false"

sed -i \
    -e "/^[[:space:]]*url:/ s/:.*/: \"${DBURL}\"/"\
    -e "/^[[:space:]]*user:/ s/:.*/: \"${DBUSER}\"/"\
    -e "/^[[:space:]]*password:/ s/:.*/: \"${DBPASS}\"/"\
    -e "/^[[:space:]]*user = / s/=.*/= \"${EMAIL_USER}\"/"\
    -e "/^[[:space:]]*password = / s/=.*/= \"${EMAIL_PASSWORD}\"/" files/application.conf

crond -l 2 -f
