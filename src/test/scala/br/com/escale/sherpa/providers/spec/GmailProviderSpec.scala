package br.com.escale.sherpa.providers.spec

import java.nio.charset.StandardCharsets
import java.nio.file.{Files, Paths}
import java.util.Properties
import javax.activation.{DataHandler, DataSource, FileDataSource}
import javax.mail._
import javax.mail.internet.{InternetAddress, MimeBodyPart, MimeMessage, MimeMultipart}

import br.com.escale.sherpa.providers.GmailProvider
import br.com.escale.sherpa.testhelpers.Faker
import com.typesafe.config.{Config, ConfigFactory}
import de.saly.javamail.mock2.{MailboxFolder, MockMailbox}
import org.scalatest.{Matchers, WordSpecLike}

object GmailProviderSpec {
  val config: Config = ConfigFactory.load()
  val username: String = config.getString("elektro.mail.credentials.user")
  val password: String = config.getString("elektro.mail.credentials.password")

  private def generateMimeMessageWithSubjectAndAttachment(subject: String, filename: String): MimeMessage = {
    val message: MimeMessage = new MimeMessage(null.asInstanceOf[Session])
    message.setSubject(subject)
    message.setText(Faker.string)
    message.setFrom(new InternetAddress(Faker.email))
    message.setRecipients(Message.RecipientType.TO,
      InternetAddress.parse(config.getString("elektro.mail.credentials.user")).asInstanceOf[Array[Address]])

    val multipart = new MimeMultipart()
    val source: DataSource = new FileDataSource(filename)

    val messageBodyPart = new MimeBodyPart()
    messageBodyPart.setDataHandler(new DataHandler(source))
    messageBodyPart.setFileName(filename)

    multipart.addBodyPart(messageBodyPart)
    message.setContent(multipart)

    message
  }
}

class GmailProviderSpec extends WordSpecLike with Matchers {
  import GmailProviderSpec._

  "GmailProvider" must {

     "be able to retrieve attachments that have given file extension from emails that have subjects matching our regex" in {
       val regex = "Base .*-Tarde".r

       val nameValidFile1 = "_" + Faker.string + ".txt"
       val nameValidFile2 = "_" + Faker.string + ".txt"
       val nameInvalidFile1 = "_" + Faker.string + ".txt.bko"
       val nameInvalidFile2 = "_" + Faker.string + ".not_txt"

       Files.write(Paths.get(nameValidFile1), Faker.string.getBytes(StandardCharsets.UTF_8))
       Files.write(Paths.get(nameValidFile2), Faker.string.getBytes(StandardCharsets.UTF_8))
       Files.write(Paths.get(nameInvalidFile1), Faker.string.getBytes(StandardCharsets.UTF_8))
       Files.write(Paths.get(nameInvalidFile2), Faker.string.getBytes(StandardCharsets.UTF_8))

       val mb: MockMailbox = MockMailbox.get(username)
       mb.getInbox.add(generateMimeMessageWithSubjectAndAttachment("Base " + Faker.string + "-Tarde", nameValidFile1))
       mb.getInbox.add(generateMimeMessageWithSubjectAndAttachment("Base" + Faker.string + "-Tarde", nameValidFile2))
       mb.getInbox.add(generateMimeMessageWithSubjectAndAttachment("Base " + Faker.string + "-Tarde", nameInvalidFile1))
       mb.getInbox.add(generateMimeMessageWithSubjectAndAttachment(Faker.string, nameInvalidFile2))

       val attachments = new GmailProvider(new Properties(), username, password).getAttachments("INBOX", regex, ".txt")

       Files.delete(Paths.get(nameValidFile1))
       Files.delete(Paths.get(nameValidFile2))
       Files.delete(Paths.get(nameInvalidFile1))
       Files.delete(Paths.get(nameInvalidFile2))

       attachments.length should be (1)
       attachments.head.file.getName should be (nameValidFile1)
     }

    "be able to move an email from one folder to another" in {
      val mockMailbox: MockMailbox = MockMailbox.get(username)
      val fromFolder: MailboxFolder = mockMailbox.getRoot.getOrAddSubFolder("from").create()
      mockMailbox.getRoot.getOrAddSubFolder("to").create()

      val provider = new GmailProvider(new Properties(), username, password)

      provider.getAttachments("from", "SUBJECT".r, ".txt").length should be (0)
      provider.getAttachments("to", "SUBJECT".r, ".txt").length should be (0)

      Files.write(Paths.get("filename.txt"), Faker.string.getBytes(StandardCharsets.UTF_8))
      fromFolder.add(generateMimeMessageWithSubjectAndAttachment("SUBJECT", "filename.txt"))

      provider.getAttachments("from", "SUBJECT".r, ".txt").length should be (1)
      provider.getAttachments("to", "SUBJECT".r, ".txt").length should be (0)

      provider.moveEmailWithSubjectToFolder("SUBJECT", "from", "to")

      provider.getAttachments("from", "SUBJECT".r, ".txt").length should be (0)
      provider.getAttachments("to", "SUBJECT".r, ".txt").length should be (1)

      Files.delete(Paths.get("filename.txt"))
    }

  }

}
