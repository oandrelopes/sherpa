//package br.com.escale.sherpa.providers.spec
//
//import javax.activation.{DataHandler, DataSource, FileDataSource}
//import javax.mail.internet.{InternetAddress, MimeBodyPart, MimeMessage, MimeMultipart}
//import javax.mail.{Address, Message, Session}
//
//import br.com.escale.sherpa.models.{ElektroMailing, ElektroMailingOwnership}
//import br.com.escale.sherpa.providers.{ElektroMailingOwnershipProvider, ElektroMailingProvider}
//import br.com.escale.sherpa.testhelpers.Faker
//import com.typesafe.config.{Config, ConfigFactory}
//import de.saly.javamail.mock2.{MailboxFolder, MockMailbox}
//import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}
//
//import scala.util.matching.Regex
//
//object ElektroMailingProviderSpec {
//  val config: Config = ConfigFactory.load()
//  val emailSubjectRegex: Regex = config.getString("elektro.mail.regex.subject").r
//  val mailingFileExtension = ".xlsx"
//
//    val validMailingPath: String = getClass.getResource("/ElektroValidMailing.xlsx").getFile
//  val invalidMailingPath: String = getClass.getResource("/ElektroInvalidMailing.xlsx").getFile
//
//  private def buildMessage(subject: String, filePath: String): MimeMessage = {
//    val message: MimeMessage = new MimeMessage(null.asInstanceOf[Session])
//    message.setSubject(subject)
//    message.setText(Faker.string)
//    message.setFrom(new InternetAddress(Faker.email))
//    message.setRecipients(Message.RecipientType.TO,
//      InternetAddress.parse(config.getString("elektro.mail.credentials.user")).asInstanceOf[Array[Address]])
//
//    val multipart = new MimeMultipart()
//    val source: DataSource = new FileDataSource(filePath)
//
//    val messageBodyPart = new MimeBodyPart()
//    messageBodyPart.setDataHandler(new DataHandler(source))
//    messageBodyPart.setFileName(filePath.split("/").last)
//
//    multipart.addBodyPart(messageBodyPart)
//    message.setContent(multipart)
//
//    message
//  }
//}
//
//class ElektroMailingProviderSpec extends WordSpecLike with Matchers with BeforeAndAfterAll {
//
//  import ElektroMailingProviderSpec._
//
//  private var mb: MailboxFolder = _
//
//  override def beforeAll(): Unit = {
//    super.beforeAll()
//
//    mb = MockMailbox.get(config.getString("elektro.mail.credentials.user")).getRoot.
//      getOrAddSubFolder(config.getString("elektro.mail.server.smtp.readyForProcessingFolder")).create()
//  }
//
//  "ElektroMailingProvider" must {
//
//    "parse only valid xlsx files" in {
//      mb.add(buildMessage("Base 19-09-2017-Tarde", validMailingPath))
//      mb.add(buildMessage("Base 01-09-2017- Tarde", validMailingPath))
//      mb.add(buildMessage("Base 12-11-2017", validMailingPath))
//      mb.add(buildMessage("Base18-09-2017-Tarde", invalidMailingPath))
//      mb.add(buildMessage("Base 20-09-2017-Tarde", invalidMailingPath))
//
//      val attachments: List[ElektroMailing] = new ElektroMailingProvider().getElektroMailing
//
//      attachments.length should be(3)
//    }
//  }
//
//  "ElektroMailingProviderOwnership" must {
//
//    "parse only valid xlsx files" in {
//      mb.add(buildMessage("Base Titularidade19-09-2018", validMailingPath))
//      mb.add(buildMessage("Base Titularidade 01-09-2018", validMailingPath))
//      mb.add(buildMessage("BaseTitularidade 12-11-2018", validMailingPath))
//      mb.add(buildMessage("BaseTitularidade18-09-2018-Tarde", invalidMailingPath))
//      mb.add(buildMessage("Base 20-09-2018-Tarde", invalidMailingPath))
//      mb.add(buildMessage("Base 20-09-2018 - Tarde", invalidMailingPath))
//
//      val attachments: List[ElektroMailingOwnership] = new ElektroMailingOwnershipProvider().getElektroOwnershipMailing
//
//      attachments.length should be(3)
//    }
//  }
//
//}
