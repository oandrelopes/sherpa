package br.com.escale.sherpa.models.aux

import java.io.File
import javax.mail.Session
import javax.mail.internet.MimeMessage

import br.com.escale.sherpa.models.Attachment

object FakeAttachmentGenerator {

  def generate(): Attachment = {
    Attachment(new MimeMessage(null.asInstanceOf[Session]), new File("/tmp/fake.attachment"))
  }

}
