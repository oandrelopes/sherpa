package br.com.escale.sherpa.helpers.spec

import javax.mail.Authenticator
import javax.mail.internet.MimeMessage

import br.com.escale.sherpa.helpers.Mail
import com.typesafe.config.{Config, ConfigFactory}
import org.scalatest.{Matchers, WordSpecLike}

class MailSpec extends WordSpecLike with Matchers {
  val config: Config = ConfigFactory.load()

  "Mail helper" must {

    "check sherpa mail properties (host, port, auth)" in {
      val props = Mail().generateMailProps()

      props.getProperty("mail.smtp.host") should be(config.getString("sherpa.mail.server.smtp.host"))
      props.getProperty("mail.smtp.socketFactory.port") should be(config.getString("sherpa.mail.server.smtp.socketFactory.port"))
      props.getProperty("mail.smtp.socketFactory.class") should be(config.getString("sherpa.mail.server.smtp.socketFactory.class"))
      props.getProperty("mail.smtp.auth") should be(config.getString("sherpa.mail.server.smtp.auth"))
      props.getProperty("mail.smtp.port") should be(config.getString("sherpa.mail.server.smtp.port"))
    }
  }
}
