package br.com.escale.sherpa.helpers.spec

import java.io.File
import java.util.Date

import br.com.escale.sherpa.helpers.{UnexpectedHeadersException, XLSXHelper}
import br.com.escale.sherpa.testhelpers.Faker
import org.apache.poi.ss.usermodel._
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.scalatest.{Matchers, WordSpecLike}

import scala.collection.JavaConverters._

object XLSXHelperSpec {
  val validMailingPath: String = getClass.getResource("/CommissionValidData.xlsx").getFile
  val invalidMailingPath: String = getClass.getResource("/CommissionInvalidData.xlsx").getFile

  def commissionMailHeader: List[String] = List("ANALISTA", "FAIXA", "COMISSAO_TOTAL", "COM_AGEND_FAIXA", "COM_GATILHO",
    "COM_DEFLATOR", "COM_AGENDADOS", "COM_FAIXA", "COM_CONVERSAO", "COM_VDU", "COM_TV", "COM_COMBO", "COM_MULTI",
    "COM_PERFORMANCE", "COM_DESISTENCIA", "COM_QUALIDADE", "IND_AGENDADOS", "IND_CONVERSAO",
    "IND_VDU", "IND_TV", "IND_COMBO", "IND_MULTI", "IND_PERFORMANCE", "IND_DESISTENCIA", "IND_QUALIDADE")
}

class XLSXHelperSpec extends WordSpecLike with Matchers {

  import XLSXHelperSpec._

  private val sampleWB: Workbook = new XSSFWorkbook()
  private val sampleSheet: Sheet = sampleWB.createSheet("SQL Result")
  private val sampleRow: Row = sampleSheet.createRow(0)

  "XLSX helper" must {

    "be able to extract data from a file with expected headers" in {
      val result = XLSXHelper.extractDataFromFileWithExpectedHeaders(new File(validMailingPath), commissionMailHeader)
      result.length should be (418)
    }

    "not throw an error when validating valid headers" in {
      val wb: Workbook = WorkbookFactory.create(new File(validMailingPath))
      val sheet = wb.getSheet("SQL Result")

      val headers: List[String] = sheet.getRow(0).asScala.map(_.getStringCellValue).toList

      XLSXHelper.validateHeaders(headers, commissionMailHeader, validMailingPath)
    }

    "throw an error when validating invalid headers" in {
      val wb: Workbook = WorkbookFactory.create(new File(invalidMailingPath))
      val sheet = wb.getSheet("SQL Result")

      val headers: List[String] = sheet.getRow(0).asScala.map(_.getStringCellValue).toList

      an [UnexpectedHeadersException] should be thrownBy XLSXHelper.validateHeaders(headers, commissionMailHeader, invalidMailingPath)
    }

    "be able to tell when a cell is empty" in {
      val row: Row = sampleSheet.createRow(Faker.intBetween(0, 100))
      XLSXHelper.isRowEmpty(row) should be (true)

      row.createCell(0, CellType.BLANK)
      XLSXHelper.isRowEmpty(row) should be (true)
    }

    "be able to tell when a cell is not empty" in {
      val row: Row = sampleSheet.createRow(Faker.intBetween(0, 100))
      val cell: Cell = row.createCell(0, CellType.NUMERIC)
      cell.setCellValue(1)

      XLSXHelper.isRowEmpty(row) should be (false)
    }

    "be able to get the value of a cell properly when it's null" in {
      val cell: Cell = null
      XLSXHelper.getCellValue(cell) should be (None)
    }

    "be able to get the value of a cell properly when it's blank" in {
      val cell: Cell = sampleRow.createCell(0, CellType.BLANK)
      XLSXHelper.getCellValue(cell) should be (None)
    }

    "be able to get the value of a cell properly when it's been errored" in {
      val cell: Cell = sampleRow.createCell(0, CellType.ERROR)
      XLSXHelper.getCellValue(cell) should be (None)
    }

    "be able to get the value of a cell properly when it contains a boolean value" in {
      val cell: Cell = sampleRow.createCell(0, CellType.BOOLEAN)
      cell.setCellValue(true)
      XLSXHelper.getCellValue(cell) should be (Some(true))
    }

    "be able to get the value of a cell properly when it contains a formula" in {
      val cell: Cell = sampleRow.createCell(0, CellType.FORMULA)
      cell.setCellFormula("SUM(A1:A2)")

      XLSXHelper.getCellValue(cell) should be (Some("SUM(A1:A2)"))
    }

    "be able to get the value of a cell properly when it contains a date" in {
      var cell: Cell = null

      val cellStyleDate: CellStyle = sampleWB.createCellStyle()
      cellStyleDate.setDataFormat(sampleWB.getCreationHelper.createDataFormat().getFormat("m/d/YY h:mm"))

      val now = new Date()
      cell = sampleRow.createCell(0)
      cell.setCellStyle(cellStyleDate)
      cell.setCellValue(now)

      XLSXHelper.getCellValue(cell) should be (Some(now))
    }

    "be able to get the value of a cell properly when it contains a numeric value" in {
      val cell: Cell = sampleRow.createCell(0, CellType.NUMERIC)
      cell.setCellValue(1)

      XLSXHelper.getCellValue(cell) should be (Some(1.0))
    }

    "be able to get the value of a cell properly when it contains a string" in {
      val cell: Cell = sampleRow.createCell(0, CellType.STRING)
      cell.setCellValue("MOYLINHO")

      XLSXHelper.getCellValue(cell) should be (Some("MOYLINHO"))
    }
  }
}
