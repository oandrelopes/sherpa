package br.com.escale.sherpa.models

case class CommissionRow(analyst: String, badge: Option[String], scheduled: Option[Double], total: Option[Double],
                         scheduledIndex: Option[Int], tvIndex: Option[Double], comboIndex: Option[Double],
                         multiIndex: Option[Double], vduIndex: Option[Double], conversionIndex: Option[Double],
                         silverBadge: Option[Double], goldBadge: Option[Double], diamondBadge: Option[Double])

object CommissionRow {
  def apply(row: Map[String, Option[Any]]): CommissionRow = {
    val analyst: String = row("ANALISTA").get.asInstanceOf[String]
    val badge: Option[String] = row("FAIXA").asInstanceOf[Option[String]]
    val scheduled: Option[Double] = row("COM_AGENDADOS").asInstanceOf[Option[Double]]
    val total: Option[Double] = row("COMISSAO_TOTAL").asInstanceOf[Option[Double]]
    val scheduleIndex: Option[Int] = row("IND_AGENDADOS").asInstanceOf[Option[Int]]
    val tvIndex: Option[Double] = row("IND_TV").asInstanceOf[Option[Double]]
    val comboIndex: Option[Double] = row("IND_COMBO").asInstanceOf[Option[Double]]
    val multiIndex: Option[Double] = row("IND_MULTI").asInstanceOf[Option[Double]]
    val vduIndex: Option[Double] = row("IND_VDU").asInstanceOf[Option[Double]]
    val conversionIndex: Option[Double] = row("IND_CONVERSAO").asInstanceOf[Option[Double]]
    val silverBadge: Option[Double] = row("SE_PRATA").asInstanceOf[Option[Double]]
    val goldBadge: Option[Double] = row("SE_OURO").asInstanceOf[Option[Double]]
    val diamondBadge: Option[Double] = row("SE_DIAMANTE").asInstanceOf[Option[Double]]

    CommissionRow.apply(analyst, badge, scheduled, total, scheduleIndex, tvIndex, comboIndex, multiIndex, vduIndex,
      conversionIndex, silverBadge, goldBadge, diamondBadge)
  }
}
