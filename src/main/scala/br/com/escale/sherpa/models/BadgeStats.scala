package br.com.escale.sherpa.models

case class BadgeStats (attachment: Attachment, rows: List[BadgeStatsRow])

object BadgeStats {
  def apply(attachment: Attachment, rows: => List[Map[String, Option[Any]]]): BadgeStats = {
    BadgeStats.apply(attachment, rows.map(row => BadgeStatsRow(row)))
  }
}
