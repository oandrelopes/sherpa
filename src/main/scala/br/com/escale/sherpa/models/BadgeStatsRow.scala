package br.com.escale.sherpa.models

case class BadgeStatsRow (name: String, tvNumber: Option[Int], comboNumber: Option[Int], multiNumber: Option[Int],
                     quitting: Option[Double], weight: Double, description: Option[String])

object BadgeStatsRow {
  def apply(row: Map[String, Option[Any]]): BadgeStatsRow = {
    val name: String = row("Faixa").get.asInstanceOf[String]
    val tvNumber: Option[Int] = row("Tv").asInstanceOf[Option[Int]]
    val comboNumber: Option[Int] = row("Combo").asInstanceOf[Option[Int]]
    val multiNumber: Option[Int] = row("Multi").asInstanceOf[Option[Int]]
    val quitting: Option[Double] = row("Desistência").asInstanceOf[Option[Double]]
    val weight: Double = row("Peso").get.asInstanceOf[Double]
    val description: Option[String] = row("Premissa").asInstanceOf[Option[String]]

    BadgeStatsRow.apply(name, tvNumber, comboNumber, multiNumber, quitting, weight, description)
  }
}

