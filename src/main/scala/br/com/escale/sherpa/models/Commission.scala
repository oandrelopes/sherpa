package br.com.escale.sherpa.models

case class Commission(attachment: Attachment, rows: List[CommissionRow])

object Commission {
  def apply(attachment: Attachment, rows: => List[Map[String, Option[Any]]]): Commission = {
    Commission.apply(attachment, rows.map(row => CommissionRow(row)))
  }
}
