package br.com.escale.sherpa.models

import java.io.File
import javax.mail.Message

case class Attachment(email: Message, file: File)
