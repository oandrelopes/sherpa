package br.com.escale.sherpa.helpers

import java.util.{Date, Properties}
import javax.mail._
import javax.mail.internet._

import com.typesafe.config.{Config, ConfigFactory}

case class Mail() {

  private val config: Config = ConfigFactory.load()

  private def addAttachmentAndBody(content: String, message: MimeMessage): MimeMessage = {
    val messageBodyPart: MimeBodyPart = new MimeBodyPart()
    messageBodyPart.setText(content)

    val multipart: Multipart = new MimeMultipart()
    multipart.addBodyPart(messageBodyPart)
    message.setContent(multipart)
    message
  }

  private def sendEmail(from: String, to: String, cc: String, subject: String, content: String) {
    val message: MimeMessage = generateMailMessage
    message.setFrom(new InternetAddress(from))
    message.setSentDate(new Date())
    message.setSubject(subject)
    message.setRecipient(Message.RecipientType.TO, new InternetAddress(to))

    val messageWithAttachment = addAttachmentAndBody(content, message)

    if (messageWithAttachment != null ) {
      Transport.send(messageWithAttachment)
    }
  }

  private def buildAuthSession(properties: Properties, authenticator: Authenticator): Session = {
    Session.getInstance(properties, authenticator)
  }

  def generateMailProps(): Properties = {
    val properties = new Properties()
    properties.put("mail.smtp.host", config.getString("sherpa.mail.server.smtp.host"))
    properties.put("mail.smtp.socketFactory.port", config.getString("sherpa.mail.server.smtp.socketFactory.port"))
    properties.put("mail.smtp.socketFactory.class", config.getString("sherpa.mail.server.smtp.socketFactory.class"))
    properties.put("mail.smtp.auth", config.getString("sherpa.mail.server.smtp.auth"))
    properties.put("mail.smtp.port", config.getString("sherpa.mail.server.smtp.port"))
    properties
  }

  def generateCommissionMailAuthenticator(): Authenticator = {
    generateMailAuthenticator(config.getString("sherpa.mail.credentials.user"),
      config.getString("sherpa.mail.credentials.password"))
  }

  def generateMailAuthenticator(user: String, password: String): Authenticator = {
    new Authenticator() {
      override def getPasswordAuthentication = new PasswordAuthentication(user, password)
    }
  }

  def generateMailMessage: MimeMessage = {
    new MimeMessage(buildAuthSession(generateMailProps(), generateCommissionMailAuthenticator()))
  }
}
