package br.com.escale.sherpa.helpers

import java.io.File

import org.apache.poi.ss.usermodel._
import scalikejdbc.ConnectionPool.MutableMap

import scala.collection.JavaConverters._
import scala.collection.mutable


object XLSXHelper {

  @throws[UnexpectedHeadersException]
  def extractDataFromFileWithExpectedHeaders(file: File, expectedHeaders: List[String]): List[Map[String, Option[Any]]] = {
    val workbook = WorkbookFactory.create(file)
    val sheet = workbook.getSheet("Planilha1")

    var rowNumber: Int = 0
    if (file.getName.equals("Premissa_Faixa_de_Comissao.xlsx")) {
      sheet.removeRow(sheet.getRow(rowNumber))
      rowNumber = 1
    }

    val headers: List[String] = sheet.getRow(rowNumber).asScala.map(_.getStringCellValue).toList
    sheet.removeRow(sheet.getRow(rowNumber))

    validateHeaders(headers, expectedHeaders, file.getName)

    sheet.asScala.flatMap { row =>
      if (isRowEmpty(row)) {
        None
      } else {
        val rowMap: MutableMap[String, Option[Any]] = mutable.HashMap()
        for (i <- headers.indices) {
          rowMap.put(headers(i), getCellValue(row.getCell(i)))
        }
        Some(rowMap.toMap)
      }
    }.toList
  }

  @throws[UnexpectedHeadersException]
  def validateHeaders(headers: List[String], expectedHeaders: List[String], fileName: String) {
    if (headers.length != expectedHeaders.length) {
      throw UnexpectedHeadersException(s"Actual headers length in XLSX file $fileName is not equal to expected: " +
        s"got ${headers.length} wanted ${expectedHeaders.length}")
    }

    for ((expectedHeader, header) <- expectedHeaders zip headers) {
      if (!header.equals(expectedHeader)) {
        throw UnexpectedHeadersException(s"Actual headers in XLSX file $fileName were difference from expected ones")
      }
    }
  }

  def isRowEmpty(row: Row): Boolean = {
    for (i <- row.getFirstCellNum to row.getLastCellNum) {
      if (i >= 0) {
        val cell: Cell = row.getCell(i)
        if (cell != null && cell.getCellTypeEnum != CellType.BLANK) {
          return false
        }
      }
    }

    true
  }

  def getCellValue(cell: Cell): Option[Any] = {
    if (cell == null) {
      return None
    }

    cell.getCellTypeEnum match {
      case CellType.BLANK | CellType.ERROR | CellType._NONE => None
      case CellType.BOOLEAN => Some(cell.getBooleanCellValue)
      case CellType.FORMULA => Some(cell.getCellFormula)
      case CellType.NUMERIC =>
        if (DateUtil.isCellDateFormatted(cell)) {
          Some(DateUtil.getJavaDate(cell.getNumericCellValue))
        } else {
          Some(cell.getNumericCellValue)
        }
      case CellType.STRING => Some(cell.getStringCellValue)
    }
  }

}
