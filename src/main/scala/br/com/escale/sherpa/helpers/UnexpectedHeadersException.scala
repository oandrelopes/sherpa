package br.com.escale.sherpa.helpers

case class UnexpectedHeadersException (private val message: String = "", private val cause: Throwable = None.orNull)
  extends Exception(message, cause)

