package br.com.escale.sherpa.providers

import java.sql.BatchUpdateException

import br.com.escale.sherpa.helpers.{Mail, UnexpectedHeadersException, XLSXHelper}
import br.com.escale.sherpa.models.{Attachment, Commission}
import com.typesafe.config.{Config, ConfigFactory}
import scalikejdbc.{NamedDB, SQL}
import scalikejdbc.config.DBs

object CommissionProvider {
  DBs.setup('sherpa)
  private val config: Config = ConfigFactory.load()

  private val upsertQueryTemplate =
    """
        REPLACE INTO commission (
          analyst, badge, scheduled, total, schedule_index, tv_index, combo_index, multi_index, vdu_index, conversion_index,
          silver_badge, gold_badge, diamond_badge
        ) VALUES (
          ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
        )
      """

  private def commissionMailHeader: List[String] = List("ANALISTA", "FAIXA", "COMISSAO_TOTAL", "COM_AGEND_FAIXA", "COM_GATILHO",
    "COM_DEFLATOR", "COM_AGENDADOS", "COM_FAIXA", "COM_CONVERSAO", "COM_VDU", "COM_TV", "COM_COMBO", "COM_MULTI",
    "COM_PERFORMANCE", "COM_DESISTENCIA", "COM_QUALIDADE", "IND_AGENDADOS", "IND_CONVERSAO",
    "IND_VDU", "IND_TV", "IND_COMBO", "IND_MULTI", "IND_PERFORMANCE", "IND_DESISTENCIA", "IND_QUALIDADE", "SE_PRATA", "SE_OURO", "SE_DIAMANTE")
}

class CommissionProvider {
  import CommissionProvider._

  val mail: Mail = new Mail()

  private val gmailProvider: GmailProvider = new GmailProvider(mail.generateMailProps(), config.getString("sherpa.mail.credentials.user"),
    config.getString("sherpa.mail.credentials.password"))

  def getCommissionData: List[Commission] = {
    val attachments: List[Attachment] = gmailProvider.getAttachments(config.getString("sherpa.mail.server.smtp.readyForProcessingFolder"),
      config.getString("sherpa.mail.regex.commissionSubject").r, ".xlsx")

    val filesData: List[(Attachment, List[Map[String, Option[Any]]])] = attachments.flatMap { attachment =>
      try {
        val data: List[Map[String, Option[Any]]] =
          XLSXHelper.extractDataFromFileWithExpectedHeaders(attachment.file, commissionMailHeader)

        Some((attachment, data))
      } catch {
        case invalidHeaders: UnexpectedHeadersException =>
          printf(s"File ${attachment.file.getName} is a invalid Commission data XLSX: ${invalidHeaders.getMessage}\n")
          None
        case e: Exception => throw e
      }
    }

    filesData.map(x => Commission(x._1, x._2))
  }

  def markAsDone(mail: Commission): Unit = {
    val subject: String = mail.attachment.email.getSubject
    val from: String = config.getString("sherpa.mail.server.smtp.readyForProcessingFolder")
    val to: String = config.getString("sherpa.mail.server.smtp.processingDoneFolder")

    gmailProvider.moveEmailWithSubjectToFolder(subject, from, to)
  }

  @throws[BatchUpdateException]
  def store(mail: Commission): Unit = {
    val args: Seq[Seq[Any]] = mail.rows.map{ row =>
      Seq[Any](row.analyst, row.badge, row.scheduled, row.total, row.scheduledIndex, row.tvIndex, row.comboIndex,
        row.multiIndex, row.vduIndex, row.conversionIndex, row.silverBadge, row.goldBadge, row.diamondBadge)
    }

    val sql = SQL(upsertQueryTemplate)
    try {
      NamedDB('sherpa) localTx(implicit session => {
        sql.batch(args: _*).apply()
      })
    } catch {
      case err: BatchUpdateException =>
        throw err
    }
  }
}
