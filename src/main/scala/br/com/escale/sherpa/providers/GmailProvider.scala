package br.com.escale.sherpa.providers

import java.io.{File, FileOutputStream}
import java.util.Properties
import javax.mail._

import br.com.escale.sherpa.models.Attachment

import scala.util.matching.Regex

class GmailProvider(props: Properties, user: String, password: String) {
  private val emailSession: Session = Session.getDefaultInstance(props, null)
  private val store : Store = emailSession.getStore("imaps")

  def getAttachments(folderName: String, subjectPattern: Regex, fileExtension: String): List[Attachment] = {
    store.connect("smtp.gmail.com", user, password)

    val folder = store.getFolder(folderName)
    folder.open(Folder.READ_ONLY)

    val messagesWithXLSX: List[Message] =
      folder.getMessages.filter(m => subjectPattern.findFirstIn(m.getSubject).isDefined).filter { m =>
      var hasAttachment = false

      val multipart = m.getContent.asInstanceOf[Multipart]
      for(i <- 0 until multipart.getCount) {
        val part = multipart.getBodyPart(i)
        if (Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition)) {
          hasAttachment = part.getFileName.endsWith(fileExtension)
        }
      }

      hasAttachment
    }.toList

    val attachments: List[Attachment] = messagesWithXLSX.map { m =>
      var attachment: BodyPart = null

      val multipart = m.getContent.asInstanceOf[Multipart]
      (0 until multipart.getCount).iterator.takeWhile(_ => attachment == null).foreach { i =>
        val part = multipart.getBodyPart(i)
        if (Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition)) {
          attachment = part
        }
      }

      val file = new File("/tmp/" + attachment.getFileName)
      val buffer = Array[Byte](4096.toByte)
      val is = attachment.getInputStream
      val fos = new FileOutputStream(file)

      var bytesRead = is.read(buffer)
      while (bytesRead != -1) {
        fos.write(buffer, 0, bytesRead)
        bytesRead = is.read(buffer)
      }
      fos.close()

      Attachment(m, file)
    }

    folder.close(false)
    store.close()

    attachments
  }

  def moveEmailWithSubjectToFolder(subject: String, fromFolderName: String, toFolderName: String): Unit = {
    store.connect("smtp.gmail.com", user, password)

    val fromFolder = store.getFolder(fromFolderName)
    fromFolder.open(Folder.READ_WRITE)

    val toFolder = store.getFolder(toFolderName)
    toFolder.open(Folder.READ_WRITE)

    val messages: Array[Message] = fromFolder.getMessages.filter(m => m.getSubject.equalsIgnoreCase(subject))
    fromFolder.copyMessages(messages, toFolder)

    messages.foreach(_.setFlag(Flags.Flag.DELETED, true))

    fromFolder.close(true)
    toFolder.close(true)
    store.close()
  }
}
