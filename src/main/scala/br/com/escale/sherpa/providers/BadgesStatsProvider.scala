package br.com.escale.sherpa.providers

import java.sql.BatchUpdateException

import com.typesafe.config.{Config, ConfigFactory}
import br.com.escale.sherpa.helpers.{Mail, UnexpectedHeadersException, XLSXHelper}
import br.com.escale.sherpa.models.{Attachment, BadgeStats}
import scalikejdbc.{NamedDB, SQL}
import scalikejdbc.config.DBs

object BadgesStatsProvider {
  DBs.setup('sherpa)
  private val config: Config = ConfigFactory.load()

  private val upsertQueryTemplate =
    """
        REPLACE INTO badge (
          name, tv_number, combo_number, multi_number, quitting, weight, description
        ) VALUES (
          ?, ?, ?, ?, ?, ?, ?
        )
      """

  private def badgeStatsEmailHeaders:List[String] = List("Faixa", "Tv", "Combo", "Multi", "Desistência", "Peso", "Premissa")
}

class BadgesStatsProvider {
  import BadgesStatsProvider._

  private val mail : Mail = new Mail()

  private val gmailProvider: GmailProvider = new GmailProvider(mail.generateMailProps(), config.getString("sherpa.mail.credentials.user"),
    config.getString("sherpa.mail.credentials.password"))

  def getBadgeStats: List[BadgeStats] = {
    val attachments: List[Attachment] = gmailProvider.getAttachments(config.getString("sherpa.mail.server.smtp.badgeStatsProcessingFolder"),
      config.getString("sherpa.mail.regex.badgeStatsSubject").r, ".xlsx")

    val filesData: List[(Attachment, List[Map[String, Option[Any]]])] = attachments.flatMap { attachment =>
      try {
        val data: List[Map[String, Option[Any]]] =
          XLSXHelper.extractDataFromFileWithExpectedHeaders(attachment.file, badgeStatsEmailHeaders)

        Some((attachment, data))
      } catch {
        case invalidHeaders: UnexpectedHeadersException =>
          printf(s"File ${attachment.file.getName} is a invalid Commission data XLSX: ${invalidHeaders.getMessage}\n")
          None
        case e: Exception => throw e
      }
    }

    filesData.map(x => BadgeStats(x._1, x._2))
  }

  @throws[BatchUpdateException]
  def store(mail: BadgeStats): Unit = {
    val args: Seq[Seq[Any]] = mail.rows.map{ row =>
      Seq[Any](row.name, row.tvNumber, row.comboNumber, row.multiNumber, row.quitting, row.weight,
        row.description)
    }

    val sql = SQL(upsertQueryTemplate)
    try {
      NamedDB('sherpa) localTx (implicit session => {
        sql.batch(args: _*).apply()
      })
    } catch {
      case err: BatchUpdateException =>
        throw err
    }
  }

  def markAsDone(mail: BadgeStats): Unit = {
    val subject: String = mail.attachment.email.getSubject
    val from: String = config.getString("sherpa.mail.server.smtp.badgeStatsProcessingFolder")
    val to: String = config.getString("sherpa.mail.server.smtp.badgeStatsDoneFolder")

    gmailProvider.moveEmailWithSubjectToFolder(subject, from, to)
  }
}
