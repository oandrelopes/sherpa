package br.com.escale.sherpa.providers

import java.time.LocalDate
import java.time.format.DateTimeFormatter

import com.typesafe.config.{Config, ConfigFactory}

import scala.xml.{NodeSeq, XML}
import scalaj.http.{Http, HttpRequest, HttpResponse}


class HolidaysProvider {
  protected def retriveHolidaysOnCalendarioApi(year: Option[Int], state: Option[String], city: Option[String]): String = {
    val config: Config = ConfigFactory.load()
    val apiUrl: String = config.getString("elektro.calendario.url")
    val token: String = config.getString("elektro.calendario.token")
    val baseRequest: HttpRequest = Http(apiUrl)
    val response: HttpResponse[String] = baseRequest
      .param("token", token)
      .param("year", year.getOrElse("").toString)
      .param("state", state.getOrElse(""))
      .param("city", city.getOrElse(""))
      .timeout(connTimeoutMs = 2000, readTimeoutMs = 5000)
      .asString
    response.body
  }

  def getHolidays(year: Option[Int], state: Option[String], city: Option[String]): List[String] = {
    val xmlString: String = retriveHolidaysOnCalendarioApi(year: Option[Int], state: Option[String], city: Option[String])
    val xml = XML.loadString(xmlString)
    val events: NodeSeq = xml \ "event"
    val formatter: DateTimeFormatter  = DateTimeFormatter.ofPattern("dd/MM/yyyy")

    events.filter { event =>
      (event \ "type_code").text.toInt <= 3
    }.map { event =>
      LocalDate.parse((event \ "date").text, formatter).toString
    }.toList
  }
}
