package br.com.escale.sherpa

import br.com.escale.sherpa.models.{BadgeStats, Commission}
import br.com.escale.sherpa.providers.{BadgesStatsProvider, CommissionProvider}

object Sherpa extends App {
  private lazy val provider = new CommissionProvider()
  private lazy val badgeProvider = new BadgesStatsProvider()

  if (args.length != 1) {
    printHelp()
    System.exit(0)
  }

  args(0) match {
    case "import-commission-data" => importCommissionData()
    case "import-badgestats" => importBadgeStats()
    case _ => printHelp()
  }

  private def importCommissionData(): Unit = {
    val commissions: List[Commission] = provider.getCommissionData

    commissions.foreach { commission =>
      provider.store(commission)
      provider.markAsDone(commission)
    }
  }

  private def importBadgeStats(): Unit = {
    val badgeStats: List[BadgeStats] = badgeProvider.getBadgeStats

    badgeStats.foreach { badgeStat =>
      badgeProvider.store(badgeStat)
      badgeProvider.markAsDone(badgeStat)
    }
  }

  private def printHelp(): Unit = {
    print(
      """
Sherpa is Escale's commission data import for each analyst

Usage:
    sherpa [command]

Available commands are:

[import]
  - import-commission-data
  - import-badgestats

Escale 2018 - www.escale.com.br

""")
  }
}
