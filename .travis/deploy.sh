#!/bin/bash

help () {
    echo "Error: only development or master branch is supported."
}

COMMIT=${TRAVIS_COMMIT::6}

build () {
    sbt docker:publishLocal
    docker build -t ${APP_NAME}:${COMMIT} .
    eval $(aws ecr get-login --region ${AWS_REGION} --no-include-email)
    docker tag ${APP_NAME}:${COMMIT} ${AWS_ACCOUNT_NUMBER}.dkr.ecr.${AWS_REGION}.amazonaws.com/${APP_NAME}:${TAG}
    docker push ${AWS_ACCOUNT_NUMBER}.dkr.ecr.${AWS_REGION}.amazonaws.com/${APP_NAME}:${TAG}
    ecs deploy ${ECS_CLUSTER} ${ECS_SERVICE} --tag ${TAG} --region ${AWS_REGION} -e ${APP_NAME} DBNAME "${DBNAME}" -e ${APP_NAME} DBUSER "${DBUSER}" -e ${APP_NAME} DBPASS "${DBPASS}" -e ${APP_NAME} DBHOST "${DBHOST}" -e ${APP_NAME} EMAIL_USER "${EMAIL_USER}" -e ${APP_NAME} EMAIL_PASSWORD "${EMAIL_PASSWORD}"
}

case "${TRAVIS_BRANCH}" in
    "development")
        TAG=${TRAVIS_BUILD_NUMBER}
        ECS_CLUSTER=staging
        ECS_SERVICE=sherpa
        EMAIL_USER=${DEV_EMAIL_USER}
        EMAIL_PASSWORD=${DEV_EMAIL_PASSWORD}
        DBNAME=${DEV_DBNAME}
        DBUSER=${DEV_DBUSER}
        DBPASS=${DEV_DBPASS}
        DBHOST=${DEV_DBHOST}
        build
        ;;
    "master")
        TAG=latest
        ECS_CLUSTER=escale
        ECS_SERVICE=sherpa
        EMAIL_USER=${PROD_EMAIL_USER}
        EMAIL_PASSWORD=${PROD_EMAIL_PASSWORD}
        DBNAME=${PROD_DBNAME}
        DBUSER=${PROD_DBUSER}
        DBPASS=${PROD_DBPASS}
        DBHOST=${PROD_DBHOST}
        build
        ;;

    *)
        help
        exit 0
        ;;
esac
