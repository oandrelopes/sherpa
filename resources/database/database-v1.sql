CREATE DATABASE IF NOT EXISTS sherpa
  CHARSET = utf8
  COLLATE = utf8_general_ci;
USE sherpa;

CREATE TABLE IF NOT EXISTS commission (
  analyst           VARCHAR(50),
  badge             VARCHAR(10),
  scheduled         DOUBLE(10, 2),
  total             DOUBLE(10, 2),
  schedule_index    INTEGER,
  tv_index          DOUBLE(5, 2),
  combo_index       DOUBLE(5, 2),
  multi_index       DOUBLE(5, 2),
  vdu_index         DOUBLE(5, 2),
  conversion_index  DOUBLE(5, 2),
  silver_badge      DOUBLE(10, 2),
  gold_badge        DOUBLE(10, 2),
  diamond_badge     DOUBLE(10, 2),

  updated_at DATETIME NOT NULL DEFAULT current_timestamp,
  CONSTRAINT pk_commission PRIMARY KEY (analyst)
);

CREATE TABLE IF NOT EXISTS badge (
  name            VARCHAR(10),
  tv_number       INTEGER,
  combo_number    INTEGER,
  multi_number    INTEGER,
  quitting        DOUBLE(5, 2),
  weight          DOUBLE(5, 2),
  description     VARCHAR(255),

  updated_at DATETIME NOT NULL DEFAULT current_timestamp,
  CONSTRAINT pk_badge PRIMARY KEY (name)
);

CREATE TABLE IF NOT EXISTS allowed_users (
  user VARCHAR(50),
  CONSTRAINT pk_allowed_users PRIMARY KEY (user)
);
